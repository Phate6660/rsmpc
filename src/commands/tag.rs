use clap::ArgMatches;
use mpd::Client;

pub fn tag(matches: &ArgMatches, c: &mut Client) {
    let intag = matches.value_of("tag").unwrap();
    let list = c.list(mpd::tag(intag), "").unwrap();
    for x in list {
        println!("{}", x);
    }
}
